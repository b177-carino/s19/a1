
// Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
const num = 8 ** 3;
// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of 8 is ${num}`);

// Create a variable address with a value of an array containing details of an address.

const address = [258, "Washington Ave NW", "California", 90011]

// Destructure the array and print out a message with the full address using Template Literals.

const [number, street, state, zipcode] = address;
console.log(`I live at ${number} ${street}, ${state} ${zipcode}`)

// Create a variable animal with a value of an object data type with different animal details as it’s properties.

const animals = {
	name: "Lolong",
	typeAnimal: "saltwater crocodile",
	weight: 1075,
	feet: 20,
	inch: 3
};

// Destructure the object and print out a message with the details of the animal using Template Literals.

const {name, typeAnimal, weight, feet, inch} = animals;
console.log(`${animals.name} was a ${animals.typeAnimal}. He weighed at ${animals.weight}kgs with a measurement of ${animals.feet}ft ${animals.inch}inches`);


// Create an array of numbers.
const numbers = [1, 2, 3, 4, 5];

//  Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach((number) => console.log(number));

// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduced = numbers.reduce((x,y) => x + y)
console.log(reduced);

class Dog { 
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = "Zoey";
myDog.age = 2;
myDog.breed = "pomeranian";

console.log(myDog);